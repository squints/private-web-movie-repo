<?php
//squints 9 Jul 2013

//make sure future php pages contain **include("../module/securityFunctions");** Duh.
include '../db_connect.php';
//include './redirect_http.php';

define("PBKDF2_HASH_ALGORITHM", "sha512");
define("PBKDF2_ITERATIONS", 10);
define("PBKDF2_SALT_BYTE_SIZE", 24);
define("PBKDF2_HASH_BYTE_SIZE", 24);

define("HASH_SECTIONS", 4);
define("HASH_ALGORITHM_INDEX", 0);
define("HASH_ITERATION_INDEX", 1);
define("HASH_SALT_INDEX", 2);
define("HASH_PBKDF2_INDEX", 3);

function sec_session_start() {
	$session_name = 'sec_session_id'; // Set a custom session name
	$secure = false; // Set to true if using https.
	$httponly = true; // This stops javascript being able to access the session id.

	ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies.
	$cookieParams = session_get_cookie_params(); // Gets current cookies params.
	session_set_cookie_params(7200, $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
	//set cookie to autodestruct after 2h (7200s), keep current path and doman, and add no https (since we don't have a certificate) and no js
	session_name($session_name); // Sets the session name to the one set above.
	session_start(); // Start the php session
	session_regenerate_id(true); // regenerated the session, delete the old one.
}

function login($user, $password, $mysqli) {
	$stmt = $mysqli->prepare("SELECT id, username, pass FROM users WHERE username = ? LIMIT 1");
	
	if ($stmt == false)
	{
		var_dump($stmt);
		//the statement couldn't be prepared because the user doesn't exist.
	}
	$stmt->bind_param('s', $user); // Bind "username" to parameter.
	$stmt->execute(); // Execute the prepared query.
	$stmt->store_result();
	$stmt->bind_result($user_id, $username, $db_password); // get main.phpiables from result.
	$stmt->fetch();
	 
	if($stmt->num_rows == 1) { // If the user exists
		// We check if the account is locked from too many login attempts
		if(checkbrute($user_id, $mysqli) == true) {
			// Account is locked
			// Send an email to user saying their account is locked
			echo "Sorry, your account is locked from too many failed logins. Please contact your chapter's admin.";
			return false;
		} else {

			if((validate_password($password, $db_password)) === true) { // Check if the password in the database matches the password the user submitted.
				// Password is correct!
				$v = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
				$user_browser = hash('sha512',$v);
				$_SESSION['user_id'] = preg_replace("/[^0-9]+/", "", $user_id); // XSS protection as we might print this value
				$_SESSION['username'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // XSS protection as we might print this value
				$_SESSION['login_string'] = hash('sha512',$db_password.$user_browser);
				// Login successful.
				if (isset($_SESSION['user_id'], $_SESSION['username'],$_SESSION['login_string'])){
				return true;
				}
			} else {
				// Password is not correct
				// We record this attempt in the database
				$now = time();
				$mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
				return false;
			}
		}
	} else {
		// No user exists.
		return false;
	}
}

function checkbrute($user_id, $mysqli) {
	// Get timestamp of current time
	$now = time();
	// All login attempts are counted from the past 2 hours.
	$valid_attempts = $now - (2 * 60 * 60);

	if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
		$stmt->bind_param('s', $user_id);
		// Execute the prepared query.
		$stmt->execute();
		$stmt->store_result();
		// If there has been more than 5 failed logins
		if($stmt->num_rows > 5) {
			return true;
		} else {
			return false;
		}
	}
}

function login_check($mysqli) {
	// Check if all session main.phpiables are set
	if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) {
		$user_id = $_SESSION['user_id'];
		$login_string = $_SESSION['login_string'];
		$v = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
	 $user_browser = hash('sha512',$v);

	 $stmt = $mysqli->prepare("SELECT pass FROM users WHERE id = ? LIMIT 1");
	 $stmt->bind_param('s', $user_id); // Bind "$user_id" to parameter.
	 $stmt->execute(); // Execute the prepared query.
	 $stmt->store_result();

	 if($stmt->num_rows == 1) {// If the user exists
	 	$stmt->bind_result($password); // get main.phpiables from result.
	 	$stmt->fetch();
	 	$login_check = hash('sha512', $password.$user_browser);

	 	if($login_check == $login_string) {
	 		// Logged In!!!!
	 		return true;
	 	} else {
	 		// Not logged in
	 			
	 		return false;
	 	}
	 } else {
	 	// Not logged in
	 	return false;

	 }
	} else {
		// Not logged in
		return false;
	}
}




function create_hash($password)
{
	// format: algorithm:iterations:salt:hash
	$salt = base64_encode(mcrypt_create_iv(PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
	return PBKDF2_HASH_ALGORITHM . ":" . PBKDF2_ITERATIONS . ":" .  $salt . ":" .
			base64_encode(pbkdf2(
					PBKDF2_HASH_ALGORITHM,
					$password,
					$salt,
					PBKDF2_ITERATIONS,
					PBKDF2_HASH_BYTE_SIZE,
					true
			));
}

function validate_password($password, $correct_hash)
{
	$params = explode(":", $correct_hash);
	if(count($params) < HASH_SECTIONS)
		return false;
	$pbkdf2 = base64_decode($params[HASH_PBKDF2_INDEX]);
	return slow_equals(
			$pbkdf2,
			pbkdf2(
					$params[HASH_ALGORITHM_INDEX],
					$password,
					$params[HASH_SALT_INDEX],
					(int)$params[HASH_ITERATION_INDEX],
					strlen($pbkdf2),
					true
			)
	);
}

// Compares two strings $a and $b in length-constant time.
function slow_equals($a, $b)
{
	$diff = strlen($a) ^ strlen($b);
	for($i = 0; $i < strlen($a) && $i < strlen($b); $i++)
	{
		$diff |= ord($a[$i]) ^ ord($b[$i]);
	}
	return $diff === 0;
}

/*
 * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
* $algorithm - The hash algorithm to use. Recommended: SHA256
* $password - The password.
* $salt - A salt that is unique to the password.
* $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
* $key_length - The length of the derived key in bytes.
* $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
* Returns: A $key_length-byte key derived from the password and salt.
*
* This implementation of PBKDF2 was originally created by https://defuse.ca
* With improvements by http://www.variations-of-shadow.com
*/
function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false)
{
	$algorithm = strtolower($algorithm);
	if(!in_array($algorithm, hash_algos(), true))
		die('PBKDF2 ERROR: Invalid hash algorithm.');
	if($count <= 0 || $key_length <= 0)
		die('PBKDF2 ERROR: Invalid parameters.');

	$hash_length = strlen(hash($algorithm, "", true));
	$block_count = ceil($key_length / $hash_length);

	$output = "";
	for($i = 1; $i <= $block_count; $i++) {
		// $i encoded as 4 bytes, big endian.
		$last = $salt . pack("N", $i);
		// first iteration
		$last = $xorsum = hash_hmac($algorithm, $last, $password, true);
		// perform the other $count - 1 iterations
		for ($j = 1; $j < $count; $j++) {
			$xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
		}
		$output .= $xorsum;
	}

	if($raw_output)
		return substr($output, 0, $key_length);
	else
		return bin2hex(substr($output, 0, $key_length));
}

function movePage($num,$url){
	static $http = array (
			100 => "HTTP/1.1 100 Continue",
			101 => "HTTP/1.1 101 Switching Protocols",
			200 => "HTTP/1.1 200 OK",
			201 => "HTTP/1.1 201 Created",
			202 => "HTTP/1.1 202 Accepted",
			203 => "HTTP/1.1 203 Non-Authoritative Information",
			204 => "HTTP/1.1 204 No Content",
			205 => "HTTP/1.1 205 Reset Content",
			206 => "HTTP/1.1 206 Partial Content",
			300 => "HTTP/1.1 300 Multiple Choices",
			301 => "HTTP/1.1 301 Moved Permanently",
			302 => "HTTP/1.1 302 Found",
			303 => "HTTP/1.1 303 See Other",
			304 => "HTTP/1.1 304 Not Modified",
			305 => "HTTP/1.1 305 Use Proxy",
			307 => "HTTP/1.1 307 Temporary Redirect",
			400 => "HTTP/1.1 400 Bad Request",
			401 => "HTTP/1.1 401 Unauthorized",
			402 => "HTTP/1.1 402 Payment Required",
			403 => "HTTP/1.1 403 Forbidden",
			404 => "HTTP/1.1 404 Not Found",
			405 => "HTTP/1.1 405 Method Not Allowed",
			406 => "HTTP/1.1 406 Not Acceptable",
			407 => "HTTP/1.1 407 Proxy Authentication Required",
			408 => "HTTP/1.1 408 Request Time-out",
			409 => "HTTP/1.1 409 Conflict",
			410 => "HTTP/1.1 410 Gone",
			411 => "HTTP/1.1 411 Length Required",
			412 => "HTTP/1.1 412 Precondition Failed",
			413 => "HTTP/1.1 413 Request Entity Too Large",
			414 => "HTTP/1.1 414 Request-URI Too Large",
			415 => "HTTP/1.1 415 Unsupported Media Type",
			416 => "HTTP/1.1 416 Requested range not satisfiable",
			417 => "HTTP/1.1 417 Expectation Failed",
			500 => "HTTP/1.1 500 Internal Server Error",
			501 => "HTTP/1.1 501 Not Implemented",
			502 => "HTTP/1.1 502 Bad Gateway",
			503 => "HTTP/1.1 503 Service Unavailable",
			504 => "HTTP/1.1 504 Gateway Time-out"
	);
	header($http[$num]);
	header ("Location: $url");
}

function filterInput($input)
{
	$input = trim($input);
	return filter_var($input, FILTER_SANITIZE_SPECIAL_CHARS);
	
}

function validateEmail($addr)
{
	if(filter_var($addr,FILTER_VALIDATE_EMAIL)){
		return true;}
	else {
		return false;
	}
	
}
///ED.2 Added privlidge checker to block brothers from seeing certain EC position pages




///GLOBAL TODO: Test this out. Also, add wget security.
///ED.s 1 squints @creation time
///ED.s 2 squints @20 JUL 2013
//Ed.3 squints @26 JUL 2013

?>
